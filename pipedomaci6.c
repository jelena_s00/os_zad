#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main()
{
    int pd1[2],pd2[2];
    pipe(pd1);
    pipe(pd2);
    if(fork()==0)
    {
        close(pd1[1]);
        close(pd2[0]);
        char prRec[30];
        int val;

       do
        {
            val=read(pd1[0],prRec,30);
            printf("Dete je, val: %d\n",val);
            prRec[0]=toupper(prRec[0]);
            write(pd2[1],prRec,strlen(prRec)+1);
        }
         while((int)strcmp(prRec,"Kraj")!=0);
        close(pd1[0]);
        close(pd2[1]);
    }

    else
    {
        close(pd1[0]);
        close(pd2[1]);
        char rec[30];
        int val;
        char retRec[30];
       do
        {
            printf("Molimo unesite rec:\n");
            scanf("%s",rec);
            write(pd1[1],rec,strlen(rec)+1);
            val=read(pd2[0],retRec,30);
            int v;
            v=strcmp(rec,retRec);
            if(v==0)
                printf("Identicne reci\n");
            else
                printf("Ne ide bajo moj!\n");
        }
         while((int)strcmp(rec,"Kraj")!=0);
        wait(NULL);
        close(pd1[1]);
        close(pd2[0]);
    }
}
