#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <sys/ipc.h> //medjuprocesna komunikacija

#define MUTEX_KEY 10100
#define PROC1_KEY 10101
#define PROC2_KEY 10102
#define DUZINA 80

union semun
{
    int val;
    struct semid_ds *buf;
    ushort *array;
};

int main()
{
    int mutexid,proc1id,proc2id;
    union semun semopts;
    char linija[DUZINA];
    FILE* read1;
    FILE* read2;
    FILE* wrt;

    //operacije nad semaforima
    struct sembuf sem_lock={0,-1,0};
    struct sembuf sem_unlock= {0,1,0};

    //kreiranje semafora
    mutexid=semget((key_t)MUTEX_KEY,1,0666|IPC_CREAT);
    proc1id=semget((key_t)PROC1_KEY,1,0666|IPC_CREAT);
    proc2id=semget((key_t)PROC2_KEY,1,0666|IPC_CREAT);

    //inicijalizacija semafora
    semopts.val=1;
    semctl(mutexid,0,SETVAL,semopts);
    semopts.val=1;
    semctl(proc1id,0,SETVAL,semopts);
    semopts.val=0;
    semctl(proc2id,0,SETVAL,semopts);

    if (fork()==0)
    {
        mutexid=semget((key_t)MUTEX_KEY,1,0666|IPC_CREAT);
        proc1id=semget((key_t)PROC1_KEY,1,0666|IPC_CREAT);
        proc2id=semget((key_t)PROC2_KEY,1,0666|IPC_CREAT);

        read1=fopen("prva.txt","r");

        while(!feof(read1))
        {
            fgets(linija,DUZINA,read1);

            semop(proc1id,&sem_lock,1);
            semop(mutexid,&sem_lock,1);

            wrt=fopen("zbir.txt","a");

            fprintf(wrt,"%s",linija);

            fclose(wrt);

            semop(mutexid,&sem_unlock,1);
            semop(proc2id,&sem_unlock,1);
        }
        fclose(read1);

    }
    else
    {
        mutexid=semget((key_t)MUTEX_KEY,1,0666|IPC_CREAT);
        proc1id=semget((key_t)PROC1_KEY,1,0666|IPC_CREAT);
        proc2id=semget((key_t)PROC2_KEY,1,0666|IPC_CREAT);

        read2=fopen("druga.txt","r");

        while(!feof(read2))
        {
            fgets(linija,DUZINA,read2);

            semop(proc2id,&sem_lock,1);
            semop(mutexid,&sem_lock,1);

            wrt=fopen("zbir.txt","a");

            fprintf(wrt,"%s",linija);

            fclose(wrt);

            semop(mutexid,&sem_unlock,1);
            semop(proc1id,&sem_unlock,1);
        }

        fclose(read2);

        wait(NULL);
        semctl(mutexid,0,IPC_RMID,0);
        semctl(proc1id,0,IPC_RMID,0);
        semctl(proc2id,0,IPC_RMID,0);
    }
}




