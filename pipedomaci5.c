#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

int main()
{
    int pd[2];
    pipe(pd);

    if(fork()==0)
    {
        close(pd[1]);
        int b,i;
        for(i=0;i<20;i++)
        {
            read(pd[0],&b,sizeof(int));
            if(b%3==0)
                printf("%d\n",b);
        }
        close(pd[0]);
    }
    else
    {
        close(pd[0]);
        int i,br;
        for(i=0;i<20;i++)
        {
            br=(rand()%(199+1-100))+100;
            write(pd[1],&br,sizeof(int));
        }
        wait(NULL);
        close(pd[1]);
    }
}
